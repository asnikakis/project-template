var gulp = require('gulp'),
	gutil = require('gulp-util'),
	sass = require('gulp-sass'),
	imagemin = require('gulp-imagemin'),
	uglify = require('gulp-uglify'),
	rename = require('gulp-rename'),
	jshint = require('gulp-jshint'),
	clean = require('gulp-clean'),
	prefix = require('gulp-autoprefixer'),
	cssbeautify = require('gulp-cssbeautify'),
	preprocess = require('gulp-preprocess'),
	replace = require('gulp-batch-replace'),
	template = require('gulp-template'),
	connect = require('connect'),
	proxy = require('proxy-middleware'),
	open = require('open'),
	path = require('path'),
	nurl = require('url'),
	tap = require('gulp-tap'),
	campaign = require('./src/resources.json');

/*
 * Environment
 */

var srcPath = './src',
	distPath = './campaign',
	pubPath = './sitecore'
	hbfUrl = 'http://stage.hbfstage.tundra.com.au/',
	paths = {
		src: {
			styles: srcPath + '/styles/',
			scripts: srcPath + '/scripts/',
			images: srcPath + '/images/'
		},
		dist: {
			styles: distPath + '/styles',
			scripts: distPath + '/scripts',
			images: distPath + '/images'
		},
		pub: {
			styles: pubPath + '/styles',
			scripts: pubPath + '/scripts',
			images: pubPath + '/images'
		}
	};

var lrport = 35729,
	srvport = 8000,
	lr;

process.env.TARGET = 'dev';

function errorHandler(error) {
	gutil.beep();
	gutil.log(
		gutil.colors.bgGreen(error.plugin),
		gutil.colors.bgRed(error.message.replace(/\r?\n|\r/g, ''))
	);
};

gulp.task('clean', function () {
	gulp.src(distPath + '/**', { read: false })
		.pipe(clean())
		.on('error', errorHandler);
});

/*
 * Styles
 */

gulp.task('styles', function() {
	runStyles();

});

gulp.task('styles-sync', function() {
	return runStyles(false);

});

function runStyles(write) {
	write = typeof write === 'undefined' ? true : write;
	var stylesTask = gulp.src(paths.src.styles + '*.scss')
		.pipe(sass( { outputStyle: process.env.TARGET === 'sitecore' ? 'compressed' : 'nested' }).on('error', errorHandler))
		.pipe(prefix('last 1 version', '> 1%', 'ie 8', 'ie 7').on('error', errorHandler))
		//.pipe(cssbeautify())
		.pipe(write ? gulp.dest(paths.dist.styles) : tap(function(file) {
			var contents = file._contents.toString(),
				filename = path.basename(file.path).replace('.css','');
			if(contents.length) {
				process.env['CSS_' + filename.toUpperCase()] = contents;
			}
		}));
	if(!write) return stylesTask;
}

/*
 * Scripts
 */

gulp.task('scripts', ['lint'], function() {
	runScripts();
});

gulp.task('scripts-sync', ['lint'], function() {
	return runScripts(false);
});

function runScripts(write) {
	write = typeof write === 'undefined' ? true : write;
	var scriptsTask = gulp.src(paths.src.scripts + '*.js').on('error', errorHandler)
		.pipe(uglify({ compress: { drop_console: process.env.TARGET === 'sitecore' } }))
		.pipe(write ? gulp.dest(paths.dist.scripts) : tap(function(file) {
			var contents = file._contents.toString(),
				filename = path.basename(file.path).replace('.js','');
			if(contents.length) {
				process.env['JS_' + filename.toUpperCase()] = contents;
			}
		}));
	if(!write) return scriptsTask;
}

gulp.task('lint', function() {
	gulp.src(paths.src.scripts + '**/*.js')
		.pipe(jshint())
		.pipe(jshint.reporter('default'));
});


/*
 * Images
 */

gulp.task('images', function () {
	gulp.src(paths.src.images + '**/*.{gif,jpeg,jpg,png}').on('error', errorHandler)
		.pipe(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true }))
		.pipe(gulp.dest(paths.dist.images));
});


/*
 * Templates
 */

gulp.task('process', function() {
	gulp.src('index*.html', { cwd: srcPath + '/' }).on('error', errorHandler)
		.pipe(preprocess())
		.pipe(template({
			colour: campaign.colour,
			hero: campaign.hero
		}))
		.pipe(gulp.dest(distPath));
});


/*
 * Utilities
 */

gulp.task('livereload', function(){
	lr = require('tiny-lr')();
	process.env.LIVE_RELOAD = true;

	lr.listen(lrport, function(err){
		if(err) return console.log(err);
	});

	gulp.watch(distPath + '/**/*.*').on('change', function(file) {
		var fileName = path.relative(distPath, file.path);
		lr.changed({
			body: {
				files: [fileName]
			}
		});
	});
});

gulp.task('server', ['livereload', 'watch'], function(callback) {
    var app, server, address, host, url, log=gutil.log, colors=gutil.colors;

    app = connect()
        .use(connect.logger('dev'))
        .use(connect.static(distPath))
        .use('/global', proxy(nurl.parse(hbfUrl + '/global')))
        .use('../images/global', proxy(nurl.parse(hbfUrl + '/global/hbf-build/images/global')))
        .use('/images/global', proxy(nurl.parse(hbfUrl + '/global/hbf-build/images/global')))
        .use('/Services', proxy(nurl.parse(hbfUrl + '/Services')));

    // change port and hostname to something static if you prefer
    server = require('http').createServer(app).listen(srvport /*, hostname*/);

    server.on('error', function(error) {
        log(colors.underline(colors.red('ERROR'))+' Unable to start server!');
        callback(error); // we couldn't start the server, so report it and quit gulp
    });

    server.on('listening', function() {
        address = server.address();
        host = address.address === '0.0.0.0' ? 'localhost' : address.address;
        url = 'http://' + host + ':' + address.port + '/index.html';

        log('');
        log('Started dev server at '+colors.magenta(url));
        if(gutil.env.open) {
            log('Opening dev server URL in browser');
            open(url);
        } else {
            log(colors.gray('(Run with --open to automatically open URL on startup)'));
        }
        log('');
        callback(); // we're done with this task for now
    });
});

// var isProduction = args.type === 'production';
gulp.task('setenv', function () {
	process.env.TARGET = 'sitecore';
	gutil.env.production = true;
	distPath = pubPath;
	paths.dist = paths.pub;
});


/*
 * Tasks
 */

gulp.task('build', ['setenv', 'clean', 'images', 'styles-sync', 'scripts-sync'], function () {
	gulp.src(['*.html', '!index*.html', '!_*.html'], { cwd: srcPath + '/' })
		.pipe(preprocess())
		.pipe(template({ colour: campaign.colour }))
		.pipe(replace(campaign.resources))
		.pipe(gulp.dest(distPath));
});


gulp.task('default', ['clean', 'images', 'styles', 'scripts', 'process'], function () {
});

gulp.task('watch', ['default'], function() {
	gulp.watch(paths.src.scripts + '**/*' , ['lint', 'scripts']);
	gulp.watch(paths.src.styles + '**/*', ['styles']);
	gulp.watch(paths.src.images + '**/*', ['images']);
	gulp.watch(srcPath + '/*.html', ['process']);
});