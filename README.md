# HBF Campaign & Hero Banner Template


### Setup

1. Clone the repo to a new folder
2. From within the new folder run: `npm install`


#### Config

Campaign and Hero Banner variables are defined in `src/resources.json`.

```json
"colour" : "teal"
// used to define the campaign colour (teal, gold, green, red, or purple)

"hero" : {
    "colour" :  "#ffffff",        // hero banner background colour
    "repeat" :  "no-repeat",      // hero banner background repeat (desktop)
    "position": "center top",     // hero banner background position (desktop)
    "image" :   "images/hero.jpg" // hero banner background image (desktop)
}
```


#### Markup

There are 2 main template files that should not be edited:

```
src/index.html
src/index_mobile.html
```

that pull content from:

```
src/hero.html
src/campaign.html
```

Do not edit anything above this line in the content html files:

```
<!-- @if false !>-----DO NO EDIT ABOVE THIS LINE-----<!-- @endif -->
```


#### Styles and Scripts

The sass can be edited here:

```
src/styles/_settings.scss   // shared settings
src/styles/_settings.scss   // shared colour definitions

src/styles/hero.scss        // hero banner specific styles
src/styles/campaign.scss    // campaign body specific styles
```

You should change `src/styles/_colours.scss` to reflect the campaign colour:

```scss
$campaign:      "teal";
$primary:       $teal-dark;
$secondary:     $teal-light;
```

Any javascript needed for the page should be placed in the relevant file:

```
src/scripts/hero.js
src/scripts/campaign.js
```

**Dev**
The compiled css and js files will be included via `<link>`and `<script>` tags.

**Build**
The css and js will be inserted into the page within `<style>` and `<script>` tags (to limit the number of files you need to upload into the media library and to make it easier for the client to edit later).

If the css or javascript files are empty then no tag is included (instead of just an empty tag).


#### Images

Any images in `src/images` will be optimised and saved to `campaign/images` (dev) or `sitecore/images` (build) to be uploaded to the appropriate folder in the sitecore media library. Make sure that you publish the images after uploading them.

After uploading the images, you should update the "resources" array within `src/resources.json` to reflect the sitecore media urls:

```json
"resources" : [
    [ "/images/aaa.jpg", "~/media/bbb.ashx" ],
    [ "/images/ccc.jpg", "~/media/ddd.ashx" ]
]
```

Then run `gulp build` again to replace all ocurrences of the relative image urls with the sitecore media urls. As `gulp build` combines all css, javascript and html into file, any image references in your css and js will also be replaced.


#### Shortcodes, Variables & Includes

There are shortcodes available for several common components. Please refer to [Bug #6414 - Campaign Pages / Get a quote buttons, other common functionality](http://redmine.tundra.com.au/issues/6414) for more information about shortcodes.

A 'Get A Quote' button shortcode example is included. To enable styling of the component during development, you can have separate markup for dev and build like this:

```html
<!-- @if TARGET!='sitecore' -->
<dev>markup</dev>
<!-- @endif -->
<!-- @if TARGET='sitecore' -->
{{sitecore:shortcode}}
<!-- @endif -->
```

The campaign colour is available as a template variable to insert anywhere on the page like this:

```html
<body class='campaign-<%= colour %>'>
```

If you need to include another larger block of html, you can create a new file in `src/` prefixed with an `_` (e.g. `_partial.html`). This file will then only be available for includes like this:

```html
<!-- @include _partial.html -->
```


### Tasks

#### Watch

```
gulp watch
```

Watches all sass, html, javascript and image files for changes and runs the appropriate actions, outputting to `campaign/`.

#### Server

```
gulp server [--open]
```

The same as `watch` but also starts a LiveReload server and static file server. Add the `--open` flag to open a new window in your default browser that loads `campaign/index.html`.

The 2 templates are available at:

```
http://localhost:8000/index.html
http://localhost:8000/index_mobile.html
```

#### Build

```
gulp build
```

The build tasks replaces image references with the media urls defined in `resources.json` and moves all associated css, javascript and html into 2 files:

```
sitecore/hero.html
sitecore/campaign.html
```

These files *do not* include the header and footer from `index.html` and `index_mobile.html` so that they can be pasted straight into the `Body Html` field in sitecore.


### Sitecore

#### Hero Banner

Create a new hero banner in `content/HBF/Site Content/Banners/Hero Banners/`.

Set `Background Color`, `Background Vertical Alignment`, and `Background Stretch` to match your settings in `src/resources.json`.

Select your `Background Image` by clicking `Browse` and choosing the uploaded image from the media library.

For the `Body Html` field, click `Edit Html` then copy and past the contents of `sitecore/hero.html` into that field.

Finally, save and publish the new banner.


#### Campaign

Create a new campaign in `content/HBF/Home/Campaigns/` using the `Page - Campaign Landing` type.

Set `Campaign Theme` to match the colour set in `src/resources.json`.

Right click on the campaign then click `Insert > DataSource Folder`. Right click the new DataSource folder and click `Insert > Html Block`.

Within the new Html Block, for the `Body Html` field, click `Edit Html` then copy and past the contents of `sitecore/campaign.html` into that field.

Use `Page Editor` for the new campaign to insert the new Hero Banner.

Finally, save and publish the new campaign and sub-items.

#### Packaging

If you don't know how to package, ask Aiden or Somya.

Once you have a package, copy and paste the appropriate section from `redmine.txt` as a template to add installation and testing notes to the redmine task.


